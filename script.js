const fetchCountryUrl = fetch("https://restcountries.com/v3.1/all");

fetchCountryUrl
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    for (let i = 0; i < data.length; i++) {
      let parentDiv = document.createElement("article");
      let imageSection = document.createElement("img");
      let titleSection = document.createElement("p");
      titleSection.innerText = data[i].name.official;

      imageSection.src = data[i].flags.svg;
      parentDiv.appendChild(imageSection);
      parentDiv.appendChild(titleSection);
      parentDiv.classList.add("countryItems");
      document.getElementById("countryId").appendChild(parentDiv);
    }
  })
  .catch((error) => {
    console.error("Fetch error:", error);
  });
